FROM girish/base:0.10
MAINTAINER Johannes Zellner <johannes@nebulon.de>

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get install -y \
    bitlbee

ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

ADD supervisor-bitlbee.conf /etc/supervisor/conf.d/bitlbee.conf

# expose web server port
EXPOSE 6667
ENV LISTEN_PORT 6667

# expose bitlbee port
EXPOSE 6667

CMD [ "/app/code/start.sh" ]
