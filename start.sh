#!/bin/bash

set -eux

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i bitlbee
